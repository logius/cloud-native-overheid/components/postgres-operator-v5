# PostgreSQL cluster with PostgreSQL Operator v5

The purpose of these tasklists is to assist in creating of updating PostgreSQL clusters using the Crunchy PostgreSQL Operator: https://access.crunchydata.com/documentation/postgres-operator/v5. The installation is intended to support PostgreSQL clusters for CNO components such as GitLab, KeyCloak, Harbor, etc. The tasklist makes use of a backups to S3. 

## Main tasklist
The tasklist `main.yml` is the main entry. Its purpose is to create a normal PostgreSQL cluster or a standby cluster. All tasks are performed with the k8s API. 

The following tasks are performed by `main.yml`: 
- Create s3 bucket for pgbackrest.
- Create pgBackRest secret to make sure pgBackRest has credentials to access S3 bucket.
- Finally create PostgreSQL cluster using PostgresCluster CRD and wait until the master pod is running.

Note that all tasks are idempotent, so they can be run for updating settings in the cluster. 

## Parameters 

The tasklist supports the following parameters. 

| Parameter      | Description | 
| ----------- | ----------- |
|  s3_bucket | name of the s3 bucket |
|  s3_endpoint | fqdn of s3 (without protocol) |
|  database | name of the database  |
|  database_namespace | namespace where the cluster will be created |
|  postgres_instance | optional - allows to set storage size and replicas |

## Preconditions

The tasklist has the following preconditions: 
  - s3-secret with credentials for S3 should be available in the namespace. The secret should have `accesskey` and `secretkey` for authentication to the cluster. 
  - PostgreSQL Operator v5 should be installed.
  - A S3 object storage (f.e. a Minio instance) should be available. 

## Example usage 

For real life examples see the CNO deployment code for sonarqube and mattermost. 

Dependencies in meta.yml: 
```
dependencies:
  - name: postgres-operator-v5
    src: https://gitlab.com/logius/cloud-native-overheid/components/postgres-operator-v5/
    scm: git
    version: 1.0
    tags: [ never, install ] # The main.yml of the role postgres-operator must be skipped. 
```

Example tasks:
```
  - name: Create DB using PGO
    include_role:
      name: postgres-operator/cluster
      tasks_from: deploy_cluster.yml
    vars: 
      - s3_bucket: "{{ config.sonarqube.s3.bucket }}"
      - s3_endpoint: "{{ config.sonarqube.s3.fqdn }}"
      - database: sonar-db
      - database_namespace: "{{ config.sonarqube.namespace }}"
      - postgres_instance:
          replicas: 2
          dataVolumeClaimSpec:
            resources:
              requests:
                storage: "3G"
```