#jinja2: trim_blocks:False, lstrip_blocks: False # allow indenting of if/endif

apiVersion: postgres-operator.crunchydata.com/v1beta1
kind: PostgresCluster
metadata:
  name: {{ database }}
  namespace: {{ database_namespace }}
spec:
  image: {{ image_registry }}/crunchydata/crunchy-postgres:{{ pg_image_version | default('ubi8-13.6-1') }}
  postgresVersion: {{ pg_version | default('13') }}

  {% if superuser is defined  %}
  users: 
    - name: {{ database }}
      databases:
        -  {{ database }}
      options: "SUPERUSER"
  {% endif %}

  instances:
    - name: {{ instance_name | default('pgha1') }}
      replicas: {{ instance_replicas | default('2') }}
      resources: {{ pg_resources.instance | default({}) }}
      sidecars:
        replicaCertCopy:
          resources: {{ pg_resources.sidecars | default({}) }}
      dataVolumeClaimSpec:
        accessModes:
        - "ReadWriteOnce"
        resources:
          requests:
            storage: "{{ storage_size | default('1G') }}"
            
      # spread the db pods over nodes by defining podAntiAffinity
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 1
            podAffinityTerm:
              topologyKey: kubernetes.io/hostname
              labelSelector:
                matchLabels:
                  postgres-operator.crunchydata.com/cluster: "{{ database }}"
                  postgres-operator.crunchydata.com/instance-set: "{{ instance_name | default('pgha1') }}"
{% if pgbouncer_enabled | default(false) %}
  proxy:
    pgBouncer:
      image: {{ image_registry }}/crunchydata/crunchy-pgbouncer:{{ pgbouncer_image_version | default('ubi8-1.16-2') }}
      replicas: {{ pgbouncer_replicas }}
      resources: {{ pg_resources.bouncer | default('null') }}
{% endif %}

  patroni:
    dynamicConfiguration:
      postgresql:
        pg_hba: {{ app_specific_pg_hba | default(omit) }}
        parameters: {{ default_postgresql_parameters | combine(app_specific_psql_parameters | default({})) }}

  backups:
    pgbackrest:
      configuration:
      - secret:
          name: pgbackrest-s3
      global:
        log-level-stderr: info

        # S3 backups are used for DR. We keep these 4 days. 
        repo1-path: /pgbackrest/{{ leader_namespace | default(database_namespace) }}/{{ database }}/repo1
        repo1-s3-uri-style: path
        repo1-retention-full: "4" # days
        repo1-retention-full-type: time
      
      image: {{ image_registry }}/crunchydata/crunchy-pgbackrest:{{ pgbackrest_image_version | default('ubi8-2.38-0') }}

      # manual provides support for ad hoc backups
      manual:
        repoName: repo1 # S3
        options:
          - --type=full

      repos:
        - name: repo1
          s3:
            bucket: {{ s3_bucket }}
            endpoint: {{ s3_endpoint }}
            region: "minio"
          schedules:
            full: "0 3 * * *" # Full backups every day at 3 o'clock 
            incremental: "30 */4 * * *" #  Incremental backup every 4 hours

      jobs:
        resources: {{ pg_resources.backrest | default('null') }}

  {% if standby | default(false) %}
  standby: 
    enabled: true
    repoName: repo1 # S3 
  {% endif %}

  monitoring:
    pgmonitor:
      exporter:
        image: {{ image_registry }}/crunchydata/crunchy-postgres-exporter:{{ pgexporter_image_version | default ('ubi8-5.1.0-0') }}
        resources: {{ pg_resources.exporter | default('null') }}
